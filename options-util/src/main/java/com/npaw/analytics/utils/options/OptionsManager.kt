package com.npaw.analytics.utils.options

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.util.Log

import com.google.gson.Gson

import com.npaw.youbora.lib6.plugin.Options

import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStreamReader

/**
 * Class that manages the NPAW Options loading and storing, so it can be persistent.
 */
class OptionsManager private constructor() {

    private var options: Options? = null

    private object YouboraConfigManagerHolder {
        val INSTANCE = OptionsManager()
    }

    /**
     * Loads NPAW's Options from disk.
     * @param c application context
     * @return the Options from disk
     */
    fun getOptions(c: Context): Options? {

        if (options == null) {
            // try to load from disk
            val file = File(c.filesDir, "youbora_options")

            if (file.exists()) {
                try {
                    val inputStream = BufferedReader(InputStreamReader(FileInputStream(file)))
                    // Should only have one line
                    val json = inputStream.readLine()
                    inputStream.close()
                    options = Gson().fromJson(json, Options::class.java)
                    Log.i("YouboraConfigManager", "loaded youbora config from file")
                } catch (e: Exception) {
                    Log.wtf("YouboraConfigManager", "exception when loading config to file: " + e.toString())
                }
            }
        }

        // Could not load from disk, create new options
        if (options == null) options = Options()

        return options
    }

    /**
     * Stores NPAW's Options to disk.
     * @param c application context
     */
    fun storeOptions(c: Context) {
        // Save options to disk
        if (options == null) {
            throw IllegalStateException("storeOptions called before getOptions")
        } else {
            // save to file
            val json = Gson().toJson(options)
            val file = File(c.filesDir, "youbora_options")

            try {
                val outputStream = FileOutputStream(file)
                outputStream.write(json.toByteArray(charset("UTF-8")))
                outputStream.flush()
                outputStream.close()
                Log.i("YouboraConfigManager", "saved youbora config to file")
            } catch (e: Exception) {
                Log.wtf("YouboraConfigManager", "exception when saving config to file: " + e.toString())
            }
        }
    }

    /**
     * Resets (and stores) NPAW options to its initial state
     * @param c application context
     */
    fun resetOptions(c: Context) {
        options = Options()
        storeOptions(c)
    }

    /**
     * Shows a dialog asking the user to reset the options
     * @param c application context
     */
    fun showResetDialog(c: Context, listener: OnYouboraOptionsResetListener?) {
        val builder = AlertDialog.Builder(c)

        builder.setPositiveButton("Yes") { _, _ ->
            resetOptions(c)
            listener?.onYouboraOptionsReset()
        }

        builder.setNegativeButton("No", null)
        builder.setMessage("Do you want to restore the default NPAW config?")
        builder.setTitle("Restore defaults")

        builder.show()
    }

    /**
     * Shows a new [OptionsActivity]
     * @param context the context to use
     */
    fun showConfig(context: Context) {
        val i = Intent(context, OptionsActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i)
    }

    /**
     * Interface to implement if a callback is desired upon options reset
     */
    interface OnYouboraOptionsResetListener {

        /**
         * Callback fired right after NPAW options have been reset
         */
        fun onYouboraOptionsReset()
    }

    companion object {

        /**
         * Returns the singleton instance
         * @return the instance
         */
        @JvmStatic
        val instance: OptionsManager
            get() = YouboraConfigManagerHolder.INSTANCE
    }
}
