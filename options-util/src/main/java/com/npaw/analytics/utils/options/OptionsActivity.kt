package com.npaw.analytics.utils.options

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import com.npaw.analytics.utils.options.databinding.OptionsActivityBinding

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.plugin.Options
import com.npaw.youbora.lib6.plugin.Plugin

/**
 * This class maps all the NPAW Options to visual widgets so they can be changed.
 * NPAW options values are loaded at onResume and stored at onPause.
 */
class OptionsActivity : Activity() {

    private lateinit var binding: OptionsActivityBinding

    private var checkBoxListener: CompoundButton.OnCheckedChangeListener? = null
    private var radioGroupListener: RadioGroup.OnCheckedChangeListener? = null
    private var textWatcher: TextWatcher? = null
    private var spinnerListener: AdapterView.OnItemSelectedListener? = null
    private var spinnerArray: Array<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OptionsActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        checkBoxListener = CompoundButton.OnCheckedChangeListener { _, _ ->
            updateUI()
            updateOptions()
        }

        radioGroupListener = RadioGroup.OnCheckedChangeListener { _, _ ->
            updateUI()
            updateOptions()
        }

        textWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) { }

            override fun afterTextChanged(s: Editable) {
                updateOptions()
            }
        }

        spinnerListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) { }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                updateOptions()
            }
        }

        spinnerArray = resources.getStringArray(R.array.streaming_protocols)

        binding.ybSpinnerStreamingProtocol.adapter = ArrayAdapter(
                this, android.R.layout.simple_spinner_dropdown_item, spinnerArray!!
        )

        // Reset options button
        binding.buttonYouboraResetConfig.setOnClickListener {
            OptionsManager.instance.showResetDialog(this@OptionsActivity,
                    object : OptionsManager.OnYouboraOptionsResetListener {
                override fun onYouboraOptionsReset() { populateUIWithOptions() }
            })
        }

        // Reset options button
        binding.buttonYouboraSendOffline.setOnClickListener {
            YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE)
            val youboraOptions = OptionsManager.instance.getOptions(this@OptionsActivity)
            val youboraPlugin = Plugin(youboraOptions, this@OptionsActivity)
            youboraPlugin.fireOfflineEvents()
        }
    }

    override fun onResume() {
        super.onResume()

        populateUIWithOptions()
        //addListeners();
    }

    /**
     * Adds listeners
     */
    private fun addListeners() {
        binding.ybCheckValueEnabled.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckValueHttps.setOnCheckedChangeListener(checkBoxListener)
        binding.ybRgMethod.setOnCheckedChangeListener(radioGroupListener)
        binding.ybCheckValueParseHls.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckValueParseCdnNode.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckValueContentIsLive.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledHost.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAccountCode.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledUsername.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAnonymousUser.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledParseCdnNameHeader.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledNetworkIP.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledNetworkISP.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledNetworkConnectionType.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledDeviceCode.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentResource.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentIsLive.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentTitle.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentTitle2.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentDuration.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentTransactionCode.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentBitrate.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentThroughput.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentRendition.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentCdn.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledContentFps.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledUserType.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledStreamingProtocol.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledOffline.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAutoDetectBackground.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam1.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam2.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam3.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam4.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam5.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam6.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam7.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam8.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam9.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledAdExtraparam10.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam1.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam2.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam3.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam4.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam5.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam6.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam7.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam8.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam9.setOnCheckedChangeListener(checkBoxListener)
        binding.ybCheckEnabledExtraparam10.setOnCheckedChangeListener(checkBoxListener)

        binding.ybTextHost.addTextChangedListener(textWatcher)
        binding.ybTextAccountCode.addTextChangedListener(textWatcher)
        binding.ybTextUsername.addTextChangedListener(textWatcher)
        binding.ybTextAnonymousUser.addTextChangedListener(textWatcher)
        binding.ybTextParseCdnNameHeader.addTextChangedListener(textWatcher)
        binding.ybTextNetworkIP.addTextChangedListener(textWatcher)
        binding.ybTextNetworkISP.addTextChangedListener(textWatcher)
        binding.ybTextNetworkConnectionType.addTextChangedListener(textWatcher)
        binding.ybTextDeviceCode.addTextChangedListener(textWatcher)
        binding.ybTextContentResource.addTextChangedListener(textWatcher)
        binding.ybTextContentTitle.addTextChangedListener(textWatcher)
        binding.ybTextContentTitle2.addTextChangedListener(textWatcher)
        binding.ybTextContentDuration.addTextChangedListener(textWatcher)
        binding.ybTextContentTransactionCode.addTextChangedListener(textWatcher)
        binding.ybTextContentBitrate.addTextChangedListener(textWatcher)
        binding.ybTextContentThroughput.addTextChangedListener(textWatcher)
        binding.ybTextContentRendition.addTextChangedListener(textWatcher)
        binding.ybTextContentCdn.addTextChangedListener(textWatcher)
        binding.ybTextContentFps.addTextChangedListener(textWatcher)
        binding.ybTextUserType.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam1.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam2.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam3.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam4.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam5.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam6.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam7.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam8.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam9.addTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam10.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam1.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam2.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam3.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam4.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam5.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam6.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam7.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam8.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam9.addTextChangedListener(textWatcher)
        binding.ybTextExtraparam10.addTextChangedListener(textWatcher)

        binding.ybSpinnerStreamingProtocol.onItemSelectedListener = spinnerListener
    }

    override fun onPause() {
        storeOptions()
        removeListeners()

        super.onPause()
    }

    /**
     * Removes listeners
     */
    private fun removeListeners() {
        binding.ybCheckValueEnabled.setOnCheckedChangeListener(null)
        binding.ybCheckValueHttps.setOnCheckedChangeListener(null)
        binding.ybRgMethod.setOnCheckedChangeListener(null)
        binding.ybCheckValueParseHls.setOnCheckedChangeListener(null)
        binding.ybCheckValueParseCdnNode.setOnCheckedChangeListener(null)
        binding.ybCheckValueContentIsLive.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledHost.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAccountCode.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledUsername.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAnonymousUser.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledParseCdnNameHeader.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledNetworkIP.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledNetworkISP.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledNetworkConnectionType.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledDeviceCode.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentResource.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentIsLive.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentTitle.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentTitle2.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentDuration.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentTransactionCode.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentBitrate.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentThroughput.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentRendition.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentCdn.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledContentFps.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledUserType.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledStreamingProtocol.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledOffline.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAutoDetectBackground.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam1.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam2.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam3.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam4.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam5.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam6.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam7.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam8.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam9.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledAdExtraparam10.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam1.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam2.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam3.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam4.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam5.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam6.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam7.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam8.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam9.setOnCheckedChangeListener(null)
        binding.ybCheckEnabledExtraparam10.setOnCheckedChangeListener(null)

        binding.ybTextHost.removeTextChangedListener(textWatcher)
        binding.ybTextAccountCode.removeTextChangedListener(textWatcher)
        binding.ybTextUsername.removeTextChangedListener(textWatcher)
        binding.ybTextAnonymousUser.removeTextChangedListener(textWatcher)
        binding.ybTextParseCdnNameHeader.removeTextChangedListener(textWatcher)
        binding.ybTextNetworkIP.removeTextChangedListener(textWatcher)
        binding.ybTextNetworkISP.removeTextChangedListener(textWatcher)
        binding.ybTextNetworkConnectionType.removeTextChangedListener(textWatcher)
        binding.ybTextDeviceCode.removeTextChangedListener(textWatcher)
        binding.ybTextContentResource.removeTextChangedListener(textWatcher)
        binding.ybTextContentTitle.removeTextChangedListener(textWatcher)
        binding.ybTextContentTitle2.removeTextChangedListener(textWatcher)
        binding.ybTextContentDuration.removeTextChangedListener(textWatcher)
        binding.ybTextContentTransactionCode.removeTextChangedListener(textWatcher)
        binding.ybTextContentBitrate.removeTextChangedListener(textWatcher)
        binding.ybTextContentThroughput.removeTextChangedListener(textWatcher)
        binding.ybTextContentRendition.removeTextChangedListener(textWatcher)
        binding.ybTextContentCdn.removeTextChangedListener(textWatcher)
        binding.ybTextContentFps.removeTextChangedListener(textWatcher)
        binding.ybTextUserType.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam1.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam2.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam3.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam4.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam5.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam6.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam7.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam8.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam9.removeTextChangedListener(textWatcher)
        binding.ybTextAdExtraparam10.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam1.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam2.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam3.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam4.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam5.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam6.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam7.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam8.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam9.removeTextChangedListener(textWatcher)
        binding.ybTextExtraparam10.removeTextChangedListener(textWatcher)

        binding.ybSpinnerStreamingProtocol.onItemSelectedListener = null
    }

    /**
     * Fill the UI with the current values in the Youbora options.
     */
    private fun populateUIWithOptions() {
        removeListeners()

        // Options -> ui
        val options = OptionsManager.instance.getOptions(this) ?: return

        binding.ybCheckValueEnabled.isChecked = options.isEnabled
        binding.ybCheckValueHttps.isChecked = options.isHttpSecure

        val method = options.method
        if (method == Options.Method.GET){
            binding.ybRgMethod.check(binding.ybRbGET.id)
        } else if (method == Options.Method.POST) {
            binding.ybRgMethod.check(binding.ybRbPOST.id)
        }

        binding.ybCheckValueParseHls.isChecked = options.isParseHls
        binding.ybCheckValueParseCdnNode.isChecked = options.isParseCdnNode
        binding.ybCheckEnabledAutoDetectBackground.isChecked = options.isAutoDetectBackground
        binding.ybCheckEnabledOffline.isChecked = options.isOffline

        val isLive = options.contentIsLive
        binding.ybCheckValueContentIsLive.isEnabled = isLive != null
        binding.ybCheckValueContentIsLive.isChecked = isLive ?: false
        binding.ybCheckEnabledContentIsLive.isChecked = isLive != null

        val streamingProtocol = options.contentStreamingProtocol
        binding.ybCheckEnabledStreamingProtocol.isChecked = streamingProtocol != null
        binding.ybSpinnerStreamingProtocol.isEnabled = streamingProtocol != null
        val position = spinnerArray!!.indexOf(streamingProtocol)
        binding.ybSpinnerStreamingProtocol.setSelection(position)

        setCheckBoxEditTextForValue(options.host, binding.ybCheckEnabledHost, binding.ybTextHost)
        setCheckBoxEditTextForValue(options.accountCode, binding.ybCheckEnabledAccountCode, binding.ybTextAccountCode)
        setCheckBoxEditTextForValue(options.username, binding.ybCheckEnabledUsername, binding.ybTextUsername)
        setCheckBoxEditTextForValue(options.userAnonymousId, binding.ybCheckEnabledAnonymousUser, binding.ybTextAnonymousUser)
        setCheckBoxEditTextForValue(options.parseCdnNameHeader, binding.ybCheckEnabledParseCdnNameHeader, binding.ybTextParseCdnNameHeader)
        setCheckBoxEditTextForValue(options.networkIP, binding.ybCheckEnabledNetworkIP, binding.ybTextNetworkIP)
        setCheckBoxEditTextForValue(options.networkIsp, binding.ybCheckEnabledNetworkISP, binding.ybTextNetworkISP)
        setCheckBoxEditTextForValue(options.networkConnectionType, binding.ybCheckEnabledNetworkConnectionType, binding.ybTextNetworkConnectionType)
        setCheckBoxEditTextForValue(options.deviceCode, binding.ybCheckEnabledDeviceCode, binding.ybTextDeviceCode)
        setCheckBoxEditTextForValue(options.contentResource, binding.ybCheckEnabledContentResource, binding.ybTextContentResource)
        setCheckBoxEditTextForValue(options.contentTitle, binding.ybCheckEnabledContentTitle, binding.ybTextContentTitle)
        setCheckBoxEditTextForValue(options.program, binding.ybCheckEnabledContentTitle2, binding.ybTextContentTitle2)
        setCheckBoxEditTextForValue(options.contentDuration, binding.ybCheckEnabledContentDuration, binding.ybTextContentDuration)
        setCheckBoxEditTextForValue(options.contentTransactionCode, binding.ybCheckEnabledContentTransactionCode, binding.ybTextContentTransactionCode)
        setCheckBoxEditTextForValue(options.contentBitrate, binding.ybCheckEnabledContentBitrate, binding.ybTextContentBitrate)
        setCheckBoxEditTextForValue(options.contentThroughput, binding.ybCheckEnabledContentThroughput, binding.ybTextContentThroughput)
        setCheckBoxEditTextForValue(options.contentRendition, binding.ybCheckEnabledContentRendition, binding.ybTextContentRendition)
        setCheckBoxEditTextForValue(options.contentCdn, binding.ybCheckEnabledContentCdn, binding.ybTextContentCdn)
        setCheckBoxEditTextForValue(options.contentFps, binding.ybCheckEnabledContentFps, binding.ybTextContentFps)
        setCheckBoxEditTextForValue(options.userType, binding.ybCheckEnabledUserType, binding.ybTextUserType)
        setCheckBoxEditTextForValue(options.adCustomDimension1, binding.ybCheckEnabledAdExtraparam1, binding.ybTextAdExtraparam1)
        setCheckBoxEditTextForValue(options.adCustomDimension2, binding.ybCheckEnabledAdExtraparam2, binding.ybTextAdExtraparam2)
        setCheckBoxEditTextForValue(options.adCustomDimension3, binding.ybCheckEnabledAdExtraparam3, binding.ybTextAdExtraparam3)
        setCheckBoxEditTextForValue(options.adCustomDimension4, binding.ybCheckEnabledAdExtraparam4, binding.ybTextAdExtraparam4)
        setCheckBoxEditTextForValue(options.adCustomDimension5, binding.ybCheckEnabledAdExtraparam5, binding.ybTextAdExtraparam5)
        setCheckBoxEditTextForValue(options.adCustomDimension6, binding.ybCheckEnabledAdExtraparam6, binding.ybTextAdExtraparam6)
        setCheckBoxEditTextForValue(options.adCustomDimension7, binding.ybCheckEnabledAdExtraparam7, binding.ybTextAdExtraparam7)
        setCheckBoxEditTextForValue(options.adCustomDimension8, binding.ybCheckEnabledAdExtraparam8, binding.ybTextAdExtraparam8)
        setCheckBoxEditTextForValue(options.adCustomDimension9, binding.ybCheckEnabledAdExtraparam9, binding.ybTextAdExtraparam9)
        setCheckBoxEditTextForValue(options.adCustomDimension10, binding.ybCheckEnabledAdExtraparam10, binding.ybTextAdExtraparam10)
        setCheckBoxEditTextForValue(options.contentCustomDimension1, binding.ybCheckEnabledExtraparam1, binding.ybTextExtraparam1)
        setCheckBoxEditTextForValue(options.contentCustomDimension2, binding.ybCheckEnabledExtraparam2, binding.ybTextExtraparam2)
        setCheckBoxEditTextForValue(options.contentCustomDimension3, binding.ybCheckEnabledExtraparam3, binding.ybTextExtraparam3)
        setCheckBoxEditTextForValue(options.contentCustomDimension4, binding.ybCheckEnabledExtraparam4, binding.ybTextExtraparam4)
        setCheckBoxEditTextForValue(options.contentCustomDimension5, binding.ybCheckEnabledExtraparam5, binding.ybTextExtraparam5)
        setCheckBoxEditTextForValue(options.contentCustomDimension6, binding.ybCheckEnabledExtraparam6, binding.ybTextExtraparam6)
        setCheckBoxEditTextForValue(options.contentCustomDimension7, binding.ybCheckEnabledExtraparam7, binding.ybTextExtraparam7)
        setCheckBoxEditTextForValue(options.contentCustomDimension8, binding.ybCheckEnabledExtraparam8, binding.ybTextExtraparam8)
        setCheckBoxEditTextForValue(options.contentCustomDimension9, binding.ybCheckEnabledExtraparam9, binding.ybTextExtraparam9)
        setCheckBoxEditTextForValue(options.contentCustomDimension10, binding.ybCheckEnabledExtraparam10, binding.ybTextExtraparam10)

        addListeners()
    }

    /**
     * Update the enabled state of the editTexts.
     */
    private fun updateUI() {
        // Update editText status depending on the checkboxes

        binding.ybTextHost.isEnabled = binding.ybCheckEnabledHost.isChecked
        binding.ybTextAccountCode.isEnabled = binding.ybCheckEnabledAccountCode.isChecked
        binding.ybTextUsername.isEnabled = binding.ybCheckEnabledUsername.isChecked
        binding.ybTextAnonymousUser.isEnabled = binding.ybCheckEnabledAnonymousUser.isChecked
        binding.ybTextParseCdnNameHeader.isEnabled = binding.ybCheckEnabledParseCdnNameHeader.isChecked
        binding.ybTextNetworkIP.isEnabled = binding.ybCheckEnabledNetworkIP.isChecked
        binding.ybTextNetworkISP.isEnabled = binding.ybCheckEnabledNetworkISP.isChecked
        binding.ybTextNetworkConnectionType.isEnabled = binding.ybCheckEnabledNetworkConnectionType.isChecked
        binding.ybTextDeviceCode.isEnabled = binding.ybCheckEnabledDeviceCode.isChecked
        binding.ybTextContentResource.isEnabled = binding.ybCheckEnabledContentResource.isChecked
        binding.ybTextContentTitle.isEnabled = binding.ybCheckEnabledContentTitle.isChecked
        binding.ybTextContentTitle2.isEnabled = binding.ybCheckEnabledContentTitle2.isChecked
        binding.ybTextContentDuration.isEnabled = binding.ybCheckEnabledContentDuration.isChecked
        binding.ybTextContentTransactionCode.isEnabled = binding.ybCheckEnabledContentTransactionCode.isChecked
        binding.ybTextContentBitrate.isEnabled = binding.ybCheckEnabledContentBitrate.isChecked
        binding.ybTextContentThroughput.isEnabled = binding.ybCheckEnabledContentThroughput.isChecked
        binding.ybTextContentRendition.isEnabled = binding.ybCheckEnabledContentRendition.isChecked
        binding.ybTextContentCdn.isEnabled = binding.ybCheckEnabledContentCdn.isChecked
        binding.ybTextContentFps.isEnabled = binding.ybCheckEnabledContentFps.isChecked
        binding.ybTextUserType.isEnabled = binding.ybCheckEnabledUserType.isChecked
        binding.ybTextAdExtraparam1.isEnabled = binding.ybCheckEnabledAdExtraparam1.isChecked
        binding.ybTextAdExtraparam2.isEnabled = binding.ybCheckEnabledAdExtraparam2.isChecked
        binding.ybTextAdExtraparam3.isEnabled = binding.ybCheckEnabledAdExtraparam3.isChecked
        binding.ybTextAdExtraparam4.isEnabled = binding.ybCheckEnabledAdExtraparam4.isChecked
        binding.ybTextAdExtraparam5.isEnabled = binding.ybCheckEnabledAdExtraparam5.isChecked
        binding.ybTextAdExtraparam6.isEnabled = binding.ybCheckEnabledAdExtraparam6.isChecked
        binding.ybTextAdExtraparam7.isEnabled = binding.ybCheckEnabledAdExtraparam7.isChecked
        binding.ybTextAdExtraparam8.isEnabled = binding.ybCheckEnabledAdExtraparam8.isChecked
        binding.ybTextAdExtraparam9.isEnabled = binding.ybCheckEnabledAdExtraparam9.isChecked
        binding.ybTextAdExtraparam10.isEnabled = binding.ybCheckEnabledAdExtraparam10.isChecked
        binding.ybTextExtraparam1.isEnabled = binding.ybCheckEnabledExtraparam1.isChecked
        binding.ybTextExtraparam2.isEnabled = binding.ybCheckEnabledExtraparam2.isChecked
        binding.ybTextExtraparam3.isEnabled = binding.ybCheckEnabledExtraparam3.isChecked
        binding.ybTextExtraparam4.isEnabled = binding.ybCheckEnabledExtraparam4.isChecked
        binding.ybTextExtraparam5.isEnabled = binding.ybCheckEnabledExtraparam5.isChecked
        binding.ybTextExtraparam6.isEnabled = binding.ybCheckEnabledExtraparam6.isChecked
        binding.ybTextExtraparam7.isEnabled = binding.ybCheckEnabledExtraparam7.isChecked
        binding.ybTextExtraparam8.isEnabled = binding.ybCheckEnabledExtraparam8.isChecked
        binding.ybTextExtraparam9.isEnabled = binding.ybCheckEnabledExtraparam9.isChecked
        binding.ybTextExtraparam10.isEnabled = binding.ybCheckEnabledExtraparam10.isChecked

        binding.ybCheckValueContentIsLive.isEnabled = binding.ybCheckEnabledContentIsLive.isChecked

        binding.ybSpinnerStreamingProtocol.isEnabled = binding.ybCheckEnabledStreamingProtocol.isChecked
    }

    private fun setCheckBoxEditTextForValue(value: Any?, checkBox: CheckBox?, editText: EditText?) {
        var strValue: String? = null
        if (value != null) {
            strValue = value.toString()
        }
        checkBox!!.isChecked = strValue != null
        editText!!.isEnabled = strValue != null
        editText.setText(if (strValue == null) "" else strValue)
    }

    private fun updateOptions() {

        val options = OptionsManager.instance.getOptions(this) ?: return
        // UI -> Options

        // boolean
        options.isEnabled = binding.ybCheckValueEnabled.isChecked
        options.isHttpSecure = binding.ybCheckValueHttps.isChecked

        val methodId = binding.ybRgMethod.checkedRadioButtonId
        if (methodId == binding.ybRbGET.id) {
            options.method = Options.Method.GET
        } else if (methodId == binding.ybRbPOST.id) {
            options.method = Options.Method.POST
        }

        options.isParseHls = binding.ybCheckValueParseHls.isChecked
        options.isParseCdnNode = binding.ybCheckValueParseCdnNode.isChecked
        options.isAutoDetectBackground = binding.ybCheckEnabledAutoDetectBackground.isChecked
        options.isOffline = binding.ybCheckEnabledOffline.isChecked

        // Boolean
        options.contentIsLive = if (binding.ybCheckEnabledContentIsLive.isChecked) binding.ybCheckValueContentIsLive.isChecked else null

        // String
        options.host = if (binding.ybCheckEnabledHost.isChecked) binding.ybTextHost.text.toString() else null
        options.accountCode = if (binding.ybCheckEnabledAccountCode.isChecked) binding.ybTextAccountCode.text.toString() else null
        options.username = if (binding.ybCheckEnabledUsername.isChecked) binding.ybTextUsername.text.toString() else null
        options.userAnonymousId = if (binding.ybCheckEnabledAnonymousUser.isChecked) binding.ybTextAnonymousUser.text.toString() else null
        options.parseCdnNameHeader = if (binding.ybCheckEnabledParseCdnNameHeader.isChecked) binding.ybTextParseCdnNameHeader.text.toString() else null
        options.networkIP = if (binding.ybCheckEnabledNetworkIP.isChecked) binding.ybTextNetworkIP.text.toString() else null
        options.networkIsp = if (binding.ybCheckEnabledNetworkISP.isChecked) binding.ybTextNetworkISP.text.toString() else null
        options.networkConnectionType = if (binding.ybCheckEnabledNetworkConnectionType.isChecked) binding.ybTextNetworkConnectionType.text.toString() else null
        options.deviceCode = if (binding.ybCheckEnabledDeviceCode.isChecked) binding.ybTextDeviceCode.text.toString() else null
        options.contentResource = if (binding.ybCheckEnabledContentResource.isChecked) binding.ybTextContentResource.text.toString() else null
        options.contentTitle = if (binding.ybCheckEnabledContentTitle.isChecked) binding.ybTextContentTitle.text.toString() else null
        options.program = if (binding.ybCheckEnabledContentTitle2.isChecked) binding.ybTextContentTitle2.text.toString() else null
        options.contentTransactionCode = if (binding.ybCheckEnabledContentTransactionCode.isChecked) binding.ybTextContentTransactionCode.text.toString() else null
        options.contentRendition = if (binding.ybCheckEnabledContentRendition.isChecked) binding.ybTextContentRendition.text.toString() else null
        options.contentCdn = if (binding.ybCheckEnabledContentCdn.isChecked) binding.ybTextContentCdn.text.toString() else null
        options.userType = if (binding.ybCheckEnabledUserType.isChecked) binding.ybTextUserType.text.toString() else null
        options.adCustomDimension1 = if (binding.ybCheckEnabledAdExtraparam1.isChecked) binding.ybTextAdExtraparam1.text.toString() else null
        options.adCustomDimension2 = if (binding.ybCheckEnabledAdExtraparam2.isChecked) binding.ybTextAdExtraparam2.text.toString() else null
        options.adCustomDimension3 = if (binding.ybCheckEnabledAdExtraparam3.isChecked) binding.ybTextAdExtraparam3.text.toString() else null
        options.adCustomDimension4 = if (binding.ybCheckEnabledAdExtraparam4.isChecked) binding.ybTextAdExtraparam4.text.toString() else null
        options.adCustomDimension5 = if (binding.ybCheckEnabledAdExtraparam5.isChecked) binding.ybTextAdExtraparam5.text.toString() else null
        options.adCustomDimension6 = if (binding.ybCheckEnabledAdExtraparam6.isChecked) binding.ybTextAdExtraparam6.text.toString() else null
        options.adCustomDimension7 = if (binding.ybCheckEnabledAdExtraparam7.isChecked) binding.ybTextAdExtraparam7.text.toString() else null
        options.adCustomDimension8 = if (binding.ybCheckEnabledAdExtraparam8.isChecked) binding.ybTextAdExtraparam8.text.toString() else null
        options.adCustomDimension9 = if (binding.ybCheckEnabledAdExtraparam9.isChecked) binding.ybTextAdExtraparam9.text.toString() else null
        options.adCustomDimension10 = if (binding.ybCheckEnabledAdExtraparam10.isChecked) binding.ybTextAdExtraparam10.text.toString() else null
        options.contentCustomDimension1 = if (binding.ybCheckEnabledExtraparam1.isChecked) binding.ybTextExtraparam1.text.toString() else null
        options.contentCustomDimension2 = if (binding.ybCheckEnabledExtraparam2.isChecked) binding.ybTextExtraparam2.text.toString() else null
        options.contentCustomDimension3 = if (binding.ybCheckEnabledExtraparam3.isChecked) binding.ybTextExtraparam3.text.toString() else null
        options.contentCustomDimension4 = if (binding.ybCheckEnabledExtraparam4.isChecked) binding.ybTextExtraparam4.text.toString() else null
        options.contentCustomDimension5 = if (binding.ybCheckEnabledExtraparam5.isChecked) binding.ybTextExtraparam5.text.toString() else null
        options.contentCustomDimension6 = if (binding.ybCheckEnabledExtraparam6.isChecked) binding.ybTextExtraparam6.text.toString() else null
        options.contentCustomDimension7 = if (binding.ybCheckEnabledExtraparam7.isChecked) binding.ybTextExtraparam7.text.toString() else null
        options.contentCustomDimension8 = if (binding.ybCheckEnabledExtraparam8.isChecked) binding.ybTextExtraparam8.text.toString() else null
        options.contentCustomDimension9 = if (binding.ybCheckEnabledExtraparam9.isChecked) binding.ybTextExtraparam9.text.toString() else null
        options.contentCustomDimension10 = if (binding.ybCheckEnabledExtraparam10.isChecked) binding.ybTextExtraparam10.text.toString() else null

        options.contentStreamingProtocol = if (binding.ybCheckEnabledStreamingProtocol.isChecked) binding.ybSpinnerStreamingProtocol.selectedItem?.toString() else null

        // Numbers (Double, Long)
        var valueString: Any? = parseDouble(binding.ybTextContentDuration.text.toString())
        options.contentDuration = if (binding.ybCheckEnabledContentDuration.isChecked && valueString != null) valueString as Double? else null
        valueString = parseLong(binding.ybTextContentBitrate.text.toString())
        options.contentBitrate = if (binding.ybCheckEnabledContentBitrate.isChecked && valueString != null) valueString else null
        valueString = parseLong(binding.ybTextContentThroughput.text.toString())
        options.contentThroughput = if (binding.ybCheckEnabledContentThroughput.isChecked && valueString != null) valueString else null
        valueString = parseDouble(binding.ybTextContentFps.text.toString())
        options.contentFps = if (binding.ybCheckEnabledContentFps.isChecked && valueString != null) valueString else null
    }

    private fun parseDouble(s: String): Double? {
        var value: Double?

        try {
            value = java.lang.Double.parseDouble(s)
        } catch (e: Exception) {
            value = null
        }

        return value
    }

    private fun parseLong(s: String): Long? {
        val value = parseDouble(s)
        return value?.toLong()
    }

    private fun storeOptions() {
        // Options -> disk
        OptionsManager.instance.storeOptions(this)
    }
}
